import java.lang.*;
class Threads extends Thread{
    int num;
    Threads(int num){
        this.num=num;
    }
    public synchronized void run(){
        multip(num);
    }
    void multip(int n){
        for (int i=0;i<=5;i++){
            System.out.println(n+" X "+i+" = "+(n*i));
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
class Syncronised {
    public static void main(String[] args) {
        Threads t1=new Threads(3);
        Threads t2=new Threads(5);
        t1.start();
        t2.start();
    }
}