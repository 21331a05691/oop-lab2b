interface Dept{
    void classRooms();
    void seminarHall();
    void washRooms();
    void staffRooms();
}
class CseDept implements Dept{
    public void classRooms(){
        System.out.println("Cse dept have 12 class rooms");
    }
    public void staffRooms(){
        System.out.println("Cse dept have satff rooms");
    }
    public void seminarHall(){
        System.out.println("Cse dept has one seminar hall");
    }
    public void washRooms(){
        System.out.println("Cse dept have wash rooms");
    }
}
class Interfaces{
    public static void main(String[] args) {
        CseDept cse=new CseDept();
        cse.classRooms();
        cse.staffRooms();
        cse.washRooms();
    }
}
